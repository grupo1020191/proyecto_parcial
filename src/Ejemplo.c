

#include <unistd.h>
#include <string.h>
#include "objetos.h"

void crear_carro(void *ref, size_t tamano){
    carrito *buf = (carrito *)ref;
    printf("\nEL carro apunta a: %p",buf);
    buf->costo = 0.0;
    buf->num_llantas = 0;
    buf->marca =(char *)malloc(sizeof(char)*tamano);
    buf->color = (char *)malloc(sizeof(char)*tamano);

    //ref->color =strdup(ref->color);
    //ref->marca =strdup(ref->marca);   
    printf("\n****Carro creado****\n");
    printf("\nEL carro apunta a: %p",ref);
}


void destruir_carro(void *ref, size_t tamano){
    carrito *car = (carrito *)ref;
    printf("Entre");
    car->num_llantas = -1;
    printf("LLegue a la primera\n");
    free(car->color);
    printf("LLegue a la segunda\n");
    free(car->marca);
    printf("LLegue a la tercera\n");
    car->costo = 0.0f;
    printf("LLegue a la cuarta");
   
}


void crear_estudiante(void *ref, size_t tamano){
    estudiante* est = (estudiante *)ref;
    est->edad = 0;
    est->nombre =(char *)malloc(sizeof(char)*tamano);
    est->apellido = (char *)malloc(sizeof(char)*tamano);
    est->telefono = (char *)malloc(sizeof(char)*tamano);

    //ref->color =strdup(ref->color);
    //ref->marca =strdup(ref->marca);   
    printf("\n****Estudiante creado****\n");
}


void destruir_estudiante(void *ref, size_t tamano){
    estudiante* est = (estudiante *)ref;
    est->edad = -1;
    free(est->nombre);
    free(est->apellido);
    free(est->telefono);

   
}

void crear_animal(void *ref, size_t tamano){
    animalito* ani = (animalito*)ref;
    ani->numpatas = 0;
    ani->edad = 0;
    ani->raza =(char *)malloc(sizeof(char)*tamano);    
    ani->especie =(char *)malloc(sizeof(char)*tamano);
    ani->nombre = (char *)malloc(sizeof(char)*tamano);

    printf("\n****Animal creado****\n");
}


void destruir_animal(void *ref, size_t tamano){
    animalito* ani = (animalito*)ref;
    ani->numpatas = -1;
    ani->edad = -1;
    free(ani->raza);
    free(ani->especie);
    free(ani->nombre);   
   
}


void crear_Ejemplo(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = 0;
    int i = 0;
    for(i = 0; i< 100; i++){
    	buf->b[i] = i;
    }
    buf->msg = (char *)malloc(sizeof(char)*1000);
}

void destruir_Ejemplo(void *ref, size_t tamano){
    Ejemplo *buf = (Ejemplo *)ref;
    buf->a = -1;
    //buf->b = 0.0f;
    free(buf->msg);
}


