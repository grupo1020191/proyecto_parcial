#include <stdio.h>
#include <stdlib.h>
#include "slaballoc.h"
#include "objetos.h"




SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){
	SlabAlloc *sla = (SlabAlloc *)malloc(sizeof(SlabAlloc));
	sla->nombre=nombre;
	void* object = (void *)malloc(tamano_objeto*TAMANO_CACHE);
	sla->mem_ptr=object;
	for(int x=0;x<tamano_objeto*TAMANO_CACHE;x=x+tamano_objeto){
		constructor(object+x,tamano_objeto);
}
	sla->mem_ptr=object;
	sla->tamano_objeto=tamano_objeto;
	sla->tamano_cache=TAMANO_CACHE;
	sla->cantidad_en_uso=0;
	Slab* slabsptr=(Slab *)malloc(sizeof(Slab)*TAMANO_CACHE);
	//sla->slab_ptr=(Slab *)malloc(sizeof(Slab)*250);
	for(int n=0;n<TAMANO_CACHE;n++){
	Slab s;
	s.ptr_data=sla->mem_ptr+n*tamano_objeto;
	s.ptr_data_old=NULL;
	s.status=DISPONIBLE;
	*(slabsptr+n)=s;
}
	sla->slab_ptr=slabsptr;
	sla->constructor=constructor;
	sla->destructor=destructor;
	return sla;
}
void *obtener_cache(SlabAlloc *alloc, int crecer){
	if(alloc != NULL){
		if((alloc->cantidad_en_uso == alloc->tamano_cache)){//para ver si todos los slabs estan en uso o si hay disponibles

			if(crecer == 1){
				//se hace crecer el cache al doble de su tamanio
				alloc->mem_ptr = realloc(alloc->mem_ptr,(alloc->tamano_objeto*alloc->tamano_cache*2));//se duplica la memoria a la que apunta mem_ptr
				alloc->slab_ptr = realloc(alloc->slab_ptr,sizeof(Slab)*alloc->tamano_cache*2);// se duplica la memoria a la que apunta slab_ptr
				int i;
				for(i=0;i<alloc->tamano_cache*2;i++){//recorre la nueva region de memmoria asignada
					if(i<alloc->tamano_cache && alloc->slab_ptr[i].ptr_data_old == NULL ){
						alloc->slab_ptr[i].ptr_data_old = alloc->slab_ptr[i].ptr_data;
					}
					if(i >= alloc->tamano_cache){
						alloc->constructor(alloc->mem_ptr+(i*alloc->tamano_objeto),alloc->tamano_objeto);//se inicialiazan los nuevos objetos
						alloc->slab_ptr[i].status = DISPONIBLE;// se los marca como disponible
						alloc->slab_ptr[i].ptr_data_old = NULL;
					}
					alloc->slab_ptr[i].ptr_data = alloc->mem_ptr+(i*alloc->tamano_objeto);//se hace que apunten a la region de cada uno de los objetos
				}
				alloc->tamano_cache = alloc->tamano_cache*2;
				alloc->cantidad_en_uso++;
				alloc->slab_ptr[alloc->tamano_cache/2].status=EN_USO;
				return alloc->mem_ptr+(alloc->tamano_objeto*alloc->tamano_cache/2);
			}else{
				return NULL;
			}
		}else{// hay slabs disponibles
			int i;
			for(i=0;i<alloc->tamano_cache;i++){
				if(alloc->slab_ptr[i].status == DISPONIBLE){
					alloc->cantidad_en_uso++;
					alloc->slab_ptr[i].status = EN_USO;
					return 	alloc->slab_ptr[i].ptr_data;
				}
			}
		}
	}

	return NULL;
}
void devolver_cache(SlabAlloc *alloc, void *obj){
	Slab *s = alloc->slab_ptr;
	for(int x = 0; x<alloc->tamano_cache; x++){
		if(obj==s->ptr_data+x*alloc->tamano_objeto){
			alloc->cantidad_en_uso-=1;
			(*(s+x)).status=0;
			alloc->destructor(alloc->mem_ptr+x*alloc->tamano_objeto,alloc->tamano_objeto);
}
}
	
}

void destruir_cache(SlabAlloc *cache){
	if(cache->cantidad_en_uso==0){
		for(int x = 0; x<cache->tamano_cache;x++){
			cache->destructor(cache->mem_ptr+x*cache->tamano_objeto,cache->tamano_objeto);
}
}
	free(cache->mem_ptr);
	cache->mem_ptr=NULL;
	free(cache->slab_ptr);
	cache->slab_ptr=NULL;
	free(cache);
	cache=NULL;

}
void stats_cache(SlabAlloc *cache){
	printf("Nombre de cache:\t\t %s\n",cache->nombre);
	printf("Cantidad de slabs:\t\t %d\n",cache->tamano_cache);
	printf("Cantidad de slabs en uso:\t %d\n",cache->cantidad_en_uso);
	printf("Tamano de objeto:\t\t %ld\n\n",cache->tamano_objeto);
	
	printf("Direccion de cache->mem_ptr: \t%p\n",cache->mem_ptr);
	Slab* s = cache->slab_ptr;
	for(int n=0;n<cache->tamano_cache;n++){
		printf("Direccion ptr[%d].ptr_data:  %p\t",n,s->ptr_data+n*sizeof(Ejemplo));
		if((*(s+n)).status==1) printf("EN_USO,\t");
		else printf("DISPONIBLE,\t");
		printf("ptr[%d].ptr_data_old:\t%p\n",n,s->ptr_data_old+n*sizeof(Ejemplo));

	}
}



